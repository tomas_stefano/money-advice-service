# Resource representation that will fetch from
# the Money Advice Service site
#
class Article
  include Jeckle::Resource
  api :money_service

  attribute :english_title, String
  attribute :english_link, String

  attribute :welsh_title, String
  attribute :welsh_link, String

  # Finds an article in the money advice service site.
  #
  # @param [String] search_term The search term that will
  # be used to find articles in the site.
  #
  # @return [Article]
  #
  def self.find(search_term)
    response       = run_request('search', query: search_term).response
    article_parser = ArticleParser.new(response.body)

    Article.new(article_parser.attributes) if article_parser.has_article?
  end

  # Makes compatible with active model serializer objects.
  #
  # @param [Symbol] name the name of the attribute to be serialized
  # by the active model serializer
  #
  def read_attribute_for_serialization(name)
    attributes[name]
  end
end

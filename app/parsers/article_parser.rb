# Class responsible to receive the Money Advice Service response
# and parse the first article, fetching the english title,
# english link.
#
class ArticleParser
  # @param [String] response the expected HTML search results page.
  #
  def initialize(response)
    @response = Nokogiri::HTML(response)
  end

  # Returns true if the page has at least one search result.
  #
  # @return [true, false]
  #
  def has_article?
    first_article.present?
  end

  # Returns all the first article attributes from the page.
  #
  # @return [Hash]
  #
  def attributes
    return {} if first_article.blank?

    {
      english_title: english_title,
      english_link:  english_link,
      welsh_title:   welsh_title,
      welsh_link:    welsh_link
    }
  end

  # Parse the Nokogiri node and return the english title
  #
  # @return [String]
  #
  def english_title
    title(first_article)
  end

  # Parse the Nokogiri node and return the english link
  #
  # @return [String]
  #
  def english_link
    link(first_article)
  end

  # Parse the Nokogiri node and return the welsh title
  # if do not have welsh translation just return nil
  #
  # @return [String, NilClass]
  #
  def welsh_title
    title(welsh_translation) if welsh_translation
  end

  # Parse the Nokogiri node and return the welsh link
  # if do not have welsh translation just return nil
  #
  # @return [String, NilClass]
  #
  def welsh_link
    link(welsh_translation) if welsh_translation
  end

  private

  # @private
  #
  # Parses the body result and tries to find the first article
  #
  # @return [Nokogiri::XML::Element]
  #
  def first_article
    @first_article ||= @response.at_css('.search-results > li > h2 > a')
  end

  # @private
  #
  # Parse the page and tries to find the welsh link
  #
  # @return [Nokogiri::XML::Element]
  #
  def welsh_translation
    xpath = '//a[@lang="cy"]'
    @welsh_translation ||= Nokogiri::HTML(welsh_response).xpath(xpath).first
  end

  def welsh_response
    Faraday.get(english_link).body
  end

  def title(node)
    node.text.strip
  end

  def link(node)
    node[:href]
  end
end

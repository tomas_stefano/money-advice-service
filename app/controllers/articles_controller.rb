class ArticlesController < ActionController::API
  def show
    article = Article.find(params[:search_term])

    if article.present?
      render json: article, serializer: ArticleSerializer
    else
      message = I18n.t('article.not_found', term: params[:search_term])
      render json: { message: message }, status: :not_found
    end
  end
end

# Class responsible to serialize the article
# back to the controller.
#
class ArticleSerializer < ActiveModel::Serializer
  attributes :english, :welsh

  def english
    {
      title: object.english_title,
      link:  object.english_link
    }
  end

  def welsh
    {
      title: object.welsh_title,
      link:  object.welsh_link
    }
  end
end

require File.expand_path('../boot', __FILE__)

require 'active_model/railtie'
require 'action_controller/railtie'

Bundler.require(*Rails.groups)

module MoneyAdviceService
  class Application < Rails::Application
  end
end

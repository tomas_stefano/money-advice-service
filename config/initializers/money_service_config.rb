Jeckle.configure do |config|
  config.register :money_service do |api|
    api.base_uri     = 'https://www.moneyadviceservice.org.uk'
    api.namespaces   = { prefix: 'en' }
    api.logger       = Rails.logger
    api.read_timeout = 5

    api.middlewares do
      request :url_encoded
    end
  end
end

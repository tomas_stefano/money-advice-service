## Money Advice Service Test

This is the Money Advice Service technical Test.

This application was made to achieve the following goal:

"The user provides a search term, and gets back the english_title and url of the first article from the English site search, along with the english_title and url of the Welsh version of the article (if a Welsh version exists). The application shouldn’t have a UI, just an endpoint."

**Ruby Version used**: 2.1.2

## Clone

     git clone https://bitbucket.org/tomas_stefano/money-advice-service.git

## Setup

     bundle

## Launching Application

     rails s

## Run tests

    rspec spec

## Generate the documentation

    yardoc

## Making requests in the command line

    curl -H "Content-Type: application/json" http://localhost:3000/article?search_term=save

    curl -H "Content-Type: application/json" http://localhost:3000/article?search_term=foo

require 'spec_helper'

RSpec.describe ArticleParser do
  subject(:article_parser) { ArticleParser.new(response) }

  let(:article_response) do
    File.read(Rails.root.join('spec', 'cassettes', 'article_response.html'))
  end

  let(:no_results_response) do
    File.read(Rails.root.join('spec', 'cassettes', 'no_results_response.html'))
  end

  describe '#has_article?' do
    context 'when have article' do
      let(:response) { article_response }

      it 'returns true' do
        expect(article_parser).to have_article
      end
    end

    context 'when does not have article' do
      let(:response) { no_results_response }

      it 'returns true' do
        expect(article_parser).to_not have_article
      end
    end
  end

  describe '#attributes' do
    context 'when have article' do
      let(:link) do
        'https://www.moneyadviceservice.org.uk/en/categories/how-to-save-money'
      end

      let(:response) { article_response }

      context 'when does not have welsh article' do
        before do
          expect(article_parser).to receive(
            :welsh_response
          ).at_least(1).and_return(no_results_response)
        end

        it 'returns the english link and english title' do
          expect(article_parser.attributes).to eq(
            english_title: 'How to save money',
            english_link:  link,
            welsh_title:   nil,
            welsh_link:    nil
          )
        end
      end

      context 'when have welsh article' do
        let(:welsh_response) do
          File.read(Rails.root.join('spec', 'cassettes', 'welsh_response.html'))
        end

        before do
          expect(article_parser).to receive(
            :welsh_response
          ).and_return(welsh_response)
        end

        it 'returns the english link and english title with welsh attributes' do
          expect(article_parser.attributes).to eq(
            english_title: 'How to save money',
            english_link:  link,
            welsh_title:   'Cymraeg',
            welsh_link:    '/cy/categories/calculators'
          )
        end
      end
    end

    context 'when does not have article' do
      let(:response) { no_results_response }

      it 'returns an empty hash' do
        expect(article_parser.attributes).to eq({})
      end
    end
  end
end

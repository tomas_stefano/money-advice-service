require 'spec_helper'

RSpec.describe ArticlesController do
  describe 'GET /article?search_term=*' do
    context 'when the article exists' do
      let(:article) do
        Article.new(
          english_title: 'How to save money',
          english_link:  'https://www.moneyadviceservice.org.uk/en/categories/how-to-save-money',
          welsh_title:   'Sut i gynilo arian',
          welsh_link:    'https://www.moneyadviceservice.org.uk/cy/categories/how-to-save-money'
        )
      end

      before do
        expect(Article).to receive(:find).and_return(article)
        get :show, search_term: 'How to save money'
      end

      it 'returns the english_title of the article' do
        expect(response.body).to have_json(
          'english' => {
            'title' => 'How to save money',
            'link'  => 'https://www.moneyadviceservice.org.uk/en/categories/how-to-save-money'
          },
          'welsh' => {
            'title' => 'Sut i gynilo arian',
            'link'  => 'https://www.moneyadviceservice.org.uk/cy/categories/how-to-save-money'
          }
        )
      end

      it 'returns success response status' do
        expect(response.status).to be_ok
      end
    end

    context 'when the article does not exists' do
      before do
        expect(Article).to receive(:find).and_return(nil)
        get :show, search_term: 'How to save money'
      end

      it 'returns a message that did not found any money advice article' do
        expected_message = "Article with 'How to save money' not found."
        expect(response.body).to have_node(:message).with(expected_message)
      end

      it 'returns not found' do
        expect(response.status).to be_not_found
      end
    end
  end
end

require 'spec_helper'

RSpec.describe Article do
  describe '.find' do
    let(:request) do
      double(response: double(body: double))
    end

    before do
      allow(Article).to receive(:run_request).and_return(request)
      expect(ArticleParser).to receive(:new).and_return(article_parser)
    end

    context 'when has article' do
      let(:article_parser) do
        double(has_article?: true, attributes: {})
      end

      it 'returns the article' do
        expect(Article.find('save money')).to be_an(Article)
      end
    end

    context 'when does not have article' do
      let(:article_parser) do
        double(has_article?: false)
      end

      it 'returns nil' do
        expect(Article.find('save money')).to be nil
      end
    end
  end

  describe '#read_attribute_for_serialization' do
    let(:article) { Article.new(english_title: 'Child Benefit') }

    subject(:english_title) do
      article.read_attribute_for_serialization(:english_title)
    end

    it 'returns the attribute from attributes hash' do
      expect(english_title).to eq('Child Benefit')
    end
  end
end
